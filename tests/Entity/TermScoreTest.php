<?php
namespace App\Entity;

use App\Entity\TermScore;
use PHPUnit\Framework\TestCase;

class TermScoreTest extends TestCase
{
    /**
     * ss@dataProvider shouldReturnCorrectPositiveMentionsScoreProvider
     */
    public function testShouldReturnCorrectPositiveMentionsScore($positiveMentions, $negativeMentions, $expectedPositiveMentionsScore): void
    {
        $termScore = new TermScore();
        $termScore->setNumberOfPositiveMentions($positiveMentions);
        $termScore->setNumberOfNegativeMentions($negativeMentions);
        $this->assertEquals($expectedPositiveMentionsScore, $termScore->getPositiveMentionsScore());
    }

    public function shouldReturnCorrectPositiveMentionsScoreProvider(): array
    {
        return [
            [0, 0, -1],
            [0, 10, 0],
            [50, 0, 10],
            [5, 5, 5],
        ];
    }
}