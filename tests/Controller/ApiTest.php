<?php
namespace App\Tests\Controller;


use App\Controller\Api;
use App\Entity\TermScore;
use App\Service\TermScoreService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\ParameterBag ;
use Symfony\Component\HttpFoundation\Request;

class ApiTest extends TestCase
{
    public function testShouldReturnStatus400WhenUnsupportedTermIsGiven(): void
    {
        $termScoreServiceStub = $this->createMock(TermScoreService::class);
        $api = new Api($termScoreServiceStub);
        $queryStub = $this->createStub(ParameterBag ::class);
        $queryStub->method('get')->willReturn(false);
        $requestStub = $this->createStub(Request::class);
        $requestStub->query = $queryStub;
        $response = $api->getGithubIssuesTermScore($requestStub);
        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testShouldReturnStatus500OnException(): void
    {
        $termScoreServiceStub = $this->createMock(TermScoreService::class);
        $termScoreServiceStub->method('getGithubIssuesTermScore')->will($this->throwException(new \Exception()));
        $api = new Api($termScoreServiceStub);
        $queryStub = $this->createStub(ParameterBag ::class);
        $queryStub->method('get')->willReturn('test');
        $requestStub = $this->createStub(Request::class);
        $requestStub->query = $queryStub;
        $response = $api->getGithubIssuesTermScore($requestStub);
        $this->assertEquals(500, $response->getStatusCode());
    }

    /**
     * @dataProvider shouldReturnCorrectDataOnSuccessProvider
     */
    public function testShouldReturnCorrectDataOnSuccess($term, $score): void
    {
        $termScoreStub = $this->createStub(TermScore ::class);
        $termScoreStub->method('getPositiveMentionsScore')->willReturn($score);
        $termScoreServiceStub = $this->createMock(TermScoreService::class);
        $termScoreServiceStub->method('getGithubIssuesTermScore')->willReturn($termScoreStub);
        $api = new Api($termScoreServiceStub);
        $queryStub = $this->createStub(ParameterBag ::class);
        $queryStub->method('get')->willReturn($term);
        $requestStub = $this->createStub(Request::class);
        $requestStub->query = $queryStub;
        $response = $api->getGithubIssuesTermScore($requestStub);
        $content = json_decode($response->getContent());
        $this->assertEquals($term, $content->term ?? false);
        $this->assertEquals($score, $content->score ?? false);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function shouldReturnCorrectDataOnSuccessProvider(): array
    {
        return [
            ['test', '0'],
            ['test_2', '0'],
            ['test', '-1'],
            ['test_2', '-1'],
            ['test', '3.5'],
            ['test_3', '3.5'],
        ];
    }
}
