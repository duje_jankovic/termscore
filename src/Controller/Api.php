<?php
namespace App\Controller;

use App\Service\TermScoreService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Api extends AbstractController
{
    protected TermScoreService $termScoreService;

    public function __construct(TermScoreService $termScoreService)
    {
        $this->termScoreService = $termScoreService;
    }

    /**
     * Searches for selected term in github issues and returns average term score.
     *
     * @param string $_GET['term'] term to find score for
     *
     * @return string JSON encoded array in following format [
     *     'error' => (string) error messagge - if there is any,
     *     'term' => (string) queried term,
     *     'score' => (float) score for queried term,
     * ]
     *
     * @Route("/api/getGithubIssuesTermScore")
     */
    public function getGithubIssuesTermScore(Request $request): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $term = $request->query->get('term');
        if (!$term) {
            $response->setStatusCode('400');
            $response->setContent(json_encode([
                'error' => 'GET parameter [term] is not set ',
            ]));
            return $response;
        }
        try {
            $termScore = $this->termScoreService->getGithubIssuesTermScore($term);
        } catch (\Exception $e) {
            // TODO error logging
            $response->setStatusCode('500');
            $response->setContent(json_encode([
                'error' => 'Unable to get term score',
            ]));
            return $response;
        }
        $response->setStatusCode('200');
        $response->setContent(json_encode(['term' => $term, 'score' => $termScore->getPositiveMentionsScore()]));
        return $response;
    }
}