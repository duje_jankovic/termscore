<?php
namespace App\Controller;

use App\Service\TermScoreService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Api Version 2.0
 */
class ApiV2 extends Api
{
    public const VALID_JSON_REQUEST_CONTENT_TYPE = 'application/vnd.api+json';

    /**
     * Searches for selected term in github issues and returns average term score.
     *
     * Uses JSON:API specification.
     * Request header:
     *     Content-Type: application/vnd.api+json
     *     Accept: application/vnd.api+json
     *
     *     Media type parameters are NOT supported, in compliance to JSON:API specification!
     *
     * @param string $_GET['term'] term to find score for - mandatory
     *
     * @return string JSON encoded array, in compliance to JSON:API specification, in following format [
     *     'errors' => array of error objects, if there are any,
     *     'data' => [
     *         'type' => 'termScore',
     *         'id' => {term_name}+github/issues,
     *         'attributes' => [
     *             'term' => term_name,
     *             'score' => term_score,
     *          ]
     *     ]
     * ]
     *
     * @Route("/api/v2/getGithubIssuesTermScore")
     */
    public function getGithubIssuesTermScore(Request $request): Response
    {
        $errorResponse = $this->validateJsonApiRequestHeader($request);
        if ($errorResponse) {
            return $errorResponse;
        }
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $term = $request->query->get('term');
        if (!$term) {
            $response->setStatusCode('400');
            $response->setContent(json_encode([
                'errors' => [
                    ['status' => '400 Bad Request', 'title' => 'GET parameter [term] is not set'],
                ]
            ]));
            return $response;
        }
        try {
            $termScore = $this->termScoreService->getGithubIssuesTermScore($term);
        } catch (\Exception $e) {
            $response->setStatusCode('500');
            $response->setContent(json_encode([
                'errors' => [
                    ['status' => '500 Internal Server Error', 'title' => 'Unable to get term score'],
                ]
            ]));
            return $response;
        }
        $response->setStatusCode('200');
        $response->setContent(json_encode([
            'data' => [
                'type' => 'termScore',
                'id' => $term.'+'.TermScoreService::GITHUB_ISSUES_SOURCE,
                'attributes' => [
                    'term' => $term,
                    'score' => $termScore->getPositiveMentionsScore(),
                ],
            ],
        ]));
        return $response;
    }

    /**
     * Validates request header compliance to JSON:API specification.
     *
     * @return Response returns Responseobject  with errors or null if there are no errors
     */
    private function validateJsonApiRequestHeader(Request $request): ?Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/vnd.api+json');

        $contentType = $request->headers->get('content-type');
        $contentTypeHasMediaTypeParams = false;
        if (strpos($contentType, ';') === false) {
            $contentTypeBeforeMediaTypeParams = $contentType;
        } else {
            $contentTypeBeforeMediaTypeParams = substr($contentType, 0, strpos($contentType, ';'));
            $contentTypeHasMediaTypeParams = true;
        }

        // Checking if content type is valid, ignoring media type parameters
        if (self::VALID_JSON_REQUEST_CONTENT_TYPE != $contentTypeBeforeMediaTypeParams ) {
            $response->setStatusCode('400');
            $response->setContent(json_encode([
                'errors' => [
                    ['status' => '400 Bad Request', 'detail' => 'Unsupported Content Type'],
                ],
            ]));
            return $response;
        }

        // Checking if there are media type parameters in content type
        if ($contentTypeHasMediaTypeParams) {
            $response->setStatusCode('415');
            $response->setContent(json_encode([
                'errors' => [
                    ['status' => '415 Unsupported Media Type', 'detail' => 'Media type parameters are not supported in "Content-Type"'],
                ],
            ]));
            return $response;
        }

        // Checking if client acceptable content types contains content type: "application/vnd.api+json" without media type parameters
        $acceptableMediaTypeIsSupported = false;
        $acceptableContentTypes = explode(',', $request->headers->get('accept'));
        foreach ($acceptableContentTypes as $contentType) {
            if (self::VALID_JSON_REQUEST_CONTENT_TYPE == $contentType) {
                $acceptableMediaTypeIsSupported = true;
                break;
            }
        }

        if (!$acceptableMediaTypeIsSupported) {
            $response->setStatusCode('406');
            $response->setContent(json_encode([
                'errors' => [
                    ['status' => '406 Not Acceptable',
                     'detail' => '"Accepts" must contain "application/vnd.api+json" without media type parameters'
                    ],
                ],
            ]));
            return $response;
        }
        return null;
    }
}