<?php
namespace App\Entity;

use App\Repository\TermScoreRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TermScoreRepository::class)
 */
class TermScore
{
    /**
     * @ORM\Id @ORM\Column(type="string", length=255)
     */
    private $term;

    /**
     * @ORM\Id @ORM\Column(type="string", length=255)
     */
    private $source;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfPositiveMentions;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfNegativeMentions;

    /**
     * Positive mentions score is calculated as number of positive mentions / total number of mentions.
     * Score is then multiplied by 10 and rounded to 1 digit, making minimum score 0 and maximum 10, e.g. 3.5
     * If there are no results for wanted term, -1 will be returned as score.
     */
    public function getPositiveMentionsScore() {
        $totalMentions = $this->getNumberOfPositiveMentions() + $this->getNumberOfNegativeMentions();
        if (0 == $totalMentions) {
            return -1;
        }
        return round($this->getNumberOfPositiveMentions() / $totalMentions * 10, 1);
    }

    public function setTerm(string $term): self
    {
        $this->term = $term;
        return $this;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;
        return $this;
    }

    public function setNumberOfPositiveMentions(int $numberOfPositiveMentions): self
    {
        $this->numberOfPositiveMentions = $numberOfPositiveMentions;
        return $this;
    }

    public function setNumberOfNegativeMentions(int $numberOfNegativeMentions): self
    {
        $this->numberOfNegativeMentions = $numberOfNegativeMentions;
        return $this;
    }


    public function getTerm(): ?string
    {
        return $this->term;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function getNumberOfPositiveMentions(): ?int
    {
        return $this->numberOfPositiveMentions;
    }

    public function getNumberOfNegativeMentions(): ?int
    {
        return $this->numberOfNegativeMentions;
    }
}