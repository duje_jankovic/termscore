<?php

namespace App\Repository;

use App\Entity\TermScore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TermScore|null find($id, $lockMode = null, $lockVersion = null)
 * @method TermScore|null findOneBy(array $criteria, array $orderBy = null)
 * @method TermScore[]    findAll()
 * @method TermScore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TermScoreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TermScore::class);
    }

    /**
     * @return TermScore|null Returns TermScore object or false if no records are found
     */
    public function findByTermAndSource($term, $source)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.term = :term')
            ->andWhere('t.source = :source')
            ->setParameter('term', $term)
            ->setParameter('source', $source)
            ->getQuery()
            ->getOneOrNullResult();
        ;
    }
}
