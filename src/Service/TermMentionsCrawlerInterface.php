<?php
namespace App\Service;

interface TermMentionsCrawlerInterface {
    public function getTotalNumberOfPositiveGithubIssuesTermMentions(string $term);
    public function getTotalNumberOfNegativeGithubIssuesTermMentions(string $term);
    // Coming soon
    // public function getTotalNumberOfPositiveTwitterTermMentions(string $term);
    // public function getTotalNumberOfNegativeTwitterTermMentions(string $term);
}