<?php

namespace App\Service;

use App\Entity\TermScore;
use App\Repository\TermScoreRepository;
use Doctrine\ORM\EntityManagerInterface;

class TermScoreService implements TermScoreFetchingServiceInterface
{
    public const GITHUB_ISSUES_SOURCE = 'github/issues';

    protected EntityManagerInterface $em;
    protected TermMentionsCrawlerService $termMentionsCrawlerService;
    protected TermScoreRepository $termScoreRepository;

    public function __construct(EntityManagerInterface $em, TermScoreRepository $termScoreRepository, TermMentionsCrawlerService $termMentionsCrawlerService)
    {
        $this->em = $em;
        $this->termMentionsCrawlerService = $termMentionsCrawlerService;
        $this->termScoreRepository = $termScoreRepository;
    }

    /**
     * Fetches Github issues term score from local DB, fetches data using TermMentionsCrawlerService if there is none in local DB.
     *
     * @param string $term
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     *
     * @return TermScore object
     */
    public function getGithubIssuesTermScore(string $term): TermScore
    {
        if (!$term) {
            throw new \InvalidArgumentException('Term must not be empty');
        }
        // Check if term and source already have data in local DB
        $termScore = $this->termScoreRepository->findByTermAndSource($term, self::GITHUB_ISSUES_SOURCE);
        if (!$termScore) {
            // Fetch term data and try to insert in DB
            $termScore = $this->getGithubIssuesTermMentions($term, self::GITHUB_ISSUES_SOURCE);
            if (null === $termScore) {
                throw new \RuntimeException('Unable to get term score from source');
            }
            try {
                $this->em->persist($termScore);
                $this->em->flush();
            } catch (\Exception $e) {
                // TODO error logging
            }
            return $termScore;
        }
        return $termScore;
    }

    private function getGithubIssuesTermMentions($term): ?TermScore
    {
        $numberOfPositiveTermMentions = $this->termMentionsCrawlerService->getTotalNumberOfPositiveGithubIssuesTermMentions($term);
        if (false === $numberOfPositiveTermMentions) {
            return null;
        }
        $numberOfNegativeTermMentions = $this->termMentionsCrawlerService->getTotalNumberOfNegativeGithubIssuesTermMentions($term);
        if (false === $numberOfPositiveTermMentions) {
            return null;
        }
        $termScore = new TermScore();
        $termScore->setTerm($term);
        $termScore->setSource(self::GITHUB_ISSUES_SOURCE);
        $termScore->setNumberOfPositiveMentions($numberOfPositiveTermMentions);
        $termScore->setNumberOfNegativeMentions($numberOfNegativeTermMentions);
        return $termScore;
    }
}
