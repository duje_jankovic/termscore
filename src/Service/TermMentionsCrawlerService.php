<?php

namespace App\Service;

class TermMentionsCrawlerService implements TermMentionsCrawlerInterface
{
    public function getTotalNumberOfPositiveGithubIssuesTermMentions(string $term)
    {
        return $this->getTotalNumberOfGithubIssuesTermMentions($term, 'rocks');
    }

    public function getTotalNumberOfNegativeGithubIssuesTermMentions(string $term)
    {
        return $this->getTotalNumberOfGithubIssuesTermMentions($term, 'sucks');
    }

    private function getTotalNumberOfGithubIssuesTermMentions($term, $mentionSlug)
    {
        $endpoint = "https://api.github.com/search/issues";
        $params = ['q'=> '"'.$term.' '.$mentionSlug.'" in:issue'];
        $url = $endpoint.'?'.http_build_query($params);
        $handle = curl_init();
        curl_setopt_array($handle, array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => ['Accept: application/vnd.github.v3+json'],
            CURLOPT_USERAGENT => 'testApp',
            CURLOPT_RETURNTRANSFER => true
        ));
        $output = curl_exec($handle);
        curl_close($handle);
        if (!$output) {
            // TODO Error logging
            return false;
        }
        $decodedOutput = \json_decode($output);
        return $decodedOutput === null || !isset($decodedOutput->total_count) ? false : (int) $decodedOutput->total_count;
    }
}
