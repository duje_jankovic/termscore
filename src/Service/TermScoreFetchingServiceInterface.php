<?php
namespace App\Service;

interface TermScoreFetchingServiceInterface {
    public function getGithubIssuesTermScore(string $term);

    // Coming soon
    // public function getTwitterTermScore(string $term);
}