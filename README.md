
# termScore

- **How to get Term Github issues score**:
	- Use following URL {server_name}/api/getGithubIssuesTermScore?term={term}
		- e.g. for local development:
			- http://localhost:8000/api/getGithubIssuesTermScore?term=php
			- http://localhost:8000/api/getGithubIssuesTermScore?term=js

- **How to get Term Github issues score using API v2:**
	- API v2 uses JSON:API specification
	- Request header should contain:
		- Content-Type: application/vnd.api+json
		- Accept: application/vnd.api+json
	- Media type parameters are **NOT** supported, in compliance to JSON:API specification!
	- Use following URL {server_name}/api/v2/getGithubIssuesTermScore?term={term}
		- e.g. for local development:
			- http://localhost:8000/api/getGithubIssuesTermScore?term=php
			- http://localhost:8000/api/getGithubIssuesTermScore?term=js




- **How to setup termScore project for local development:**

	1. Install Composer from https://getcomposer.org/download/
	2. Download install Symfony CLI from https://symfony.com/download:
		- alternatively you can set up local web server following instructions on https://symfony.com/doc/current/setup/web_server_configuration.html

	3. Clone repository from https://gitlab.com/duje_jankovic/termscore.git
	4. Copy .env file and rename it to .env.local
	5. in .env.local set DATABASE_URL with database connection string
	6. Open command line in termScore project root folder
	7. Run "composer install" command
	8. Run "php bin/console doctrine:database:create" command
	9. Run "php bin/console make:migration" command
	10. Run "php bin/console doctrine:migrations:migrate" command
	11. Start server with "symfony server:start" command if Symfony CLI is installed or start local web server


- **How to run tests:**
	- Run "php bin/phpunit" from project root